﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace TestAssembly
{
    public class CallAssembly
    {
        private Assembly assembly = null;

        public const string FilePath = @"D:\Important\mail\TestClassLibrary.dll";

        private static CallAssembly instance = null;

        static CallAssembly()
        {
            //监测dll文件变化
            FileSystemWatcher watcher = new FileSystemWatcher();
            FileInfo file = new FileInfo(FilePath);
            watcher.Path = file.DirectoryName;
            watcher.Filter = file.Name;
            watcher.Changed += new FileSystemEventHandler(OnProcess);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;
        }

        public void Call()
        {
            object obj = assembly.CreateInstance("TestClassLibrary.Print");
            MethodInfo methodInfo = obj.GetType().GetMethod("Show", BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
            methodInfo.Invoke(obj, null);
        }

        private CallAssembly()
        {
            byte[] bytes = File.ReadAllBytes(FilePath);
            assembly = Assembly.Load(bytes);
        }

        public static CallAssembly Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (typeof(CallAssembly))
                    {
                        if (instance == null)
                        {
                            instance = new CallAssembly();
                        }
                    }
                }
                return instance;
            }
            set
            {
                lock (typeof(CallAssembly))
                {
                    instance = value;
                }
            }
        }

        private static void OnProcess(object source, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                Instance = null;
            }
        }
    }
}
